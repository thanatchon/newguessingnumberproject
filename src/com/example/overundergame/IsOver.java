package com.example.overundergame;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class IsOver extends Activity  implements OnClickListener{
	
	private Button button1;
	int highscore = 0;
	int myscore = 0;
	String name ="";
	String newsc ="";
	MediaPlayer mpBgm;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_is_over);
		
	
		mpBgm = MediaPlayer.create(IsOver.this, R.raw.start);
        mpBgm.setLooping(true);
        mpBgm.start();
		
		button1 = (Button) findViewById(R.id.button1);
		button1.setOnClickListener(this);
		
		Bundle score = getIntent().getExtras();
		myscore = score.getInt("life");
		
		TextView tx1 = (TextView)findViewById(R.id.textView1);
		TextView tx2 = (TextView)findViewById(R.id.textView2);
		TextView tx3 = (TextView)findViewById(R.id.textView3);
		
		tx1.setText("Your Score: "+myscore);
		
		File infile = getBaseContext().getFileStreamPath("highsc.dat");
		if(infile.exists()){
			try{
				BufferedReader reader = new BufferedReader( new FileReader (infile));
				newsc = reader.readLine().trim();
				highscore = Integer.parseInt(newsc);
				reader.close();	
			} catch(Exception e){
				
			}
		}
		
		File infile2 = getBaseContext().getFileStreamPath("name.dat");
		if(infile.exists()){
			try{
				BufferedReader reader = new BufferedReader( new FileReader (infile2));
				name = reader.readLine().trim();
				reader.close();	
			} catch(Exception e){
				
			}
		}
		
		
		MediaPlayer over = MediaPlayer.create(IsOver.this, R.raw.gameover);
		
		
		if(highscore<myscore){
			highscore = myscore;
			
		}
		
		if(myscore==0){
			over.start();
		}
		
	
		save(newsc);
		tx2.setText("High Score: "+highscore);	
		tx3.setText("Name: "+name);
		
		
	
	}
	
	
	
	
	public void save(String newsc){
		try{
			FileOutputStream outfile = openFileOutput("highsc.dat", MODE_PRIVATE);
			PrintWriter p = new PrintWriter(outfile);
			p.write(newsc);
			p.flush();
			p.close();
			outfile.close();
		} catch(FileNotFoundException e){
			Toast t = Toast.makeText(this, "Unable to Save Data", Toast.LENGTH_SHORT);
			t.show();
		} catch(IOException e){
			Toast t = Toast.makeText(this, "Unable to Save Data", Toast.LENGTH_SHORT);
			t.show(); 
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.is_over, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		mpBgm.stop();	
		
		Intent playagain = new Intent(getApplicationContext(),MainActivity.class);
		startActivity(playagain);
		
	
	}

}
