package com.example.overundergame;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {

	private Button button1;
	int randnum = (int) (Math.random() * 101) + 1;
	int life = 12;
	int input1 = -1;
	int input2 = 101;
	int oldsc = 0;
	String stsc ="";
	
	MediaPlayer mpBgm;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		button1 = (Button) findViewById(R.id.button1);
		button1.setOnClickListener(this);
		
		mpBgm = MediaPlayer.create(MainActivity.this, R.raw.start);
		mpBgm.setLooping(true);
        mpBgm.start();
		
			

		TextView t1 = (TextView) findViewById(	R.id.textView1);
		//t1.setText("X " + life+ " Secret is " + randnum);
		t1.setText("X " + life);
	

		File infile = getBaseContext().getFileStreamPath("highsc.dat");
		if(infile.exists()){
			try{
				BufferedReader reader = new BufferedReader( new FileReader (infile));
				stsc = reader.readLine().trim();
				oldsc = Integer.parseInt(stsc);
				reader.close();	
			} catch(Exception e){
				
			}
		}
		
		
	}

public void save(String newsc){
		try{
			FileOutputStream outfile = openFileOutput("highsc.dat", MODE_PRIVATE);
			PrintWriter p = new PrintWriter(outfile);
			p.write(newsc);
			p.flush();
			p.close();
			outfile.close();
		} catch(FileNotFoundException e){
			Toast t = Toast.makeText(this, "Unable to Save Data", Toast.LENGTH_SHORT);
			t.show();
		} catch(IOException e){
			Toast t = Toast.makeText(this, "Unable to Save Data", Toast.LENGTH_SHORT);
			t.show(); 
		}
	}
	public void isOver(){
		if(life<oldsc){
		Intent over = new Intent(MainActivity.this,IsOver.class);
		over.putExtra("life", life);
		startActivity(over);
		}
		else{
			stsc = String.valueOf(life);
			save(stsc);
			Intent gethighscore = new Intent(MainActivity.this,GetHighScore.class);
			gethighscore.putExtra("life", life);
			startActivity(gethighscore);

		}
		
	}
	
	

	

	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		int id = v.getId();

		EditText e1 = (EditText) findViewById(R.id.editText1);
		TextView t2 = (TextView) findViewById(R.id.textView2);
		
		
		
		try {

			String smyans = e1.getText().toString();
			int imyans = Integer.parseInt(smyans);
			if (id == R.id.button1) {

				if (imyans > 100 || imyans < 0) {
					
					MediaPlayer error1 = MediaPlayer.create(MainActivity.this, R.raw.false1);
					error1.start();
					
					Toast t = Toast.makeText(this,
							"Enter a number between 1-100 !",
							Toast.LENGTH_SHORT);
					t.show();
				} else {

					if (imyans < randnum && life > 0 && imyans > input1) {
						
						
						MediaPlayer mp1 = MediaPlayer.create(MainActivity.this, R.raw.low);
						mp1.start();
						
						input1 = imyans;

						t2.setText("More than " + imyans + " !");
						life--;

						if (life == 0) {
							
							mpBgm.stop();
							t2.setText("Your Game Is Over !");
							isOver();
						}
						
						input2 = 101;
						e1.setText("");
						

					}

					else if (imyans > randnum && life > 0 && imyans < input2) {
						
						
						MediaPlayer mp1 = MediaPlayer.create(MainActivity.this, R.raw.high);
						mp1.start();
						

						input2 = imyans;

						t2.setText("Less than " + imyans + " !");
						life--;
						input1 = -1;

						if (life == 0) {

							t2.setText("Your Game Is Over !");
							isOver();

						}
						e1.setText("");
					}

					else if (imyans == randnum) {
						
						mpBgm.stop();
						MediaPlayer correct1 = MediaPlayer.create(MainActivity.this, R.raw.true1);
						correct1.start();
						t2.setText("Correct !");
						isOver();	
						
						
					}

					else if (input1 == imyans || input2 == imyans) {
						
						MediaPlayer error1 = MediaPlayer.create(MainActivity.this, R.raw.false1);
						error1.start();
						
						Toast t = Toast.makeText(this,
								"Enter a different number !",
								Toast.LENGTH_SHORT);
						t.show();
					}

					else if (imyans < input1) {
						
						MediaPlayer error1 = MediaPlayer.create(MainActivity.this, R.raw.false1);
						error1.start();
						
						Toast t = Toast.makeText(this,
								"Enter a number that is more than " + input1,
								Toast.LENGTH_SHORT);
						t.show();
					} else if (imyans > input2) {
						
						MediaPlayer error1 = MediaPlayer.create(MainActivity.this, R.raw.false1);
						error1.start();
						
						Toast t = Toast.makeText(this,
								"Enter a number that is less than " + input2,
								Toast.LENGTH_SHORT);
						t.show();
					}
				}

			}

		} catch (Exception e) {
			MediaPlayer error1 = MediaPlayer.create(MainActivity.this, R.raw.false1);
			error1.start();
			
			Toast t = Toast.makeText(this, "Invalid Number !", Toast.LENGTH_SHORT);
			t.show();
		}

		TextView t1 = (TextView) findViewById(R.id.textView1);
		//t1.setText("X " + life+ " Secret is " + randnum);
		t1.setText("X " + life);

	}

}
