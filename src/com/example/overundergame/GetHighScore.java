package com.example.overundergame;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class GetHighScore extends Activity implements OnClickListener {

	private Button button1;
	String name = "";
	int life = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_get_high_score);
		
		button1 = (Button) findViewById(R.id.button1);
		button1.setOnClickListener(this);
		
		MediaPlayer mp1 = MediaPlayer.create(GetHighScore.this, R.raw.congrat);
		mp1.start();
		
		Bundle score = getIntent().getExtras();
		life = score.getInt("life");
		
		
		
	}
	
	public void savename(String name){
		try{
			FileOutputStream outfile2 = openFileOutput("name.dat", MODE_PRIVATE);
			PrintWriter p = new PrintWriter(outfile2);
			p.write(name);
			p.flush();
			p.close();
			outfile2.close();
		} catch(FileNotFoundException e){
			Toast t = Toast.makeText(this, "Unable to Save Data", Toast.LENGTH_SHORT);
			t.show();
		} catch(IOException e){
			Toast t = Toast.makeText(this, "Unable to Save Data", Toast.LENGTH_SHORT);
			t.show(); 
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.get_high_score, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		int id = v.getId();
		EditText e1 = (EditText) findViewById(R.id.editText1);
		String name = e1.getText().toString();
	
		if (id == R.id.button1){
			
			savename(name);
	
			Intent over = new Intent(GetHighScore.this,IsOver.class);
			over.putExtra("life", life);
			startActivity(over);
		}
		
		
		
	}

}
